#import <Cordova/CDV.h>

@interface Echo : CDVPlugin

- (void) echo:(CDVInvokedUrlCommand*)command;
- (void) datetime:(CDVInvokedUrlCommand*)command;

@end