var echo = {
  echo: function (str, successCallback) {

    cordova.exec(successCallback, 
      function(err) {
        console.log('In error callback', err);
        successCallback(err);
      }, 
      "Echo", 
      "echo", 
      [str]);   
  },
  datetime: function (successCallback) {

    cordova.exec(successCallback, 
      function(err) {
        console.log('In error callback', err);
        successCallback(err);
      }, 
      "Echo", 
      "datetime", 
      []);   
    
  }    
};

module.exports = echo;
